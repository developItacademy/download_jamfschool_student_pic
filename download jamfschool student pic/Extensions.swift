//
//  Extensions.swift
//  download jamfschool student pic
//
//  Created by Steven Hertz on 1/4/21.
//

import UIKit

extension UIImageView {

    /*
    func loadIt(url: URL) {
        let fileManager = FileManager.default
        let docURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!
        let newFileURL = docURL.appendingPathComponent("647bba344396e7c8170902bcf2e15551.jpg")
        
        if !fileManager.fileExists(atPath: newFileURL.path) {
             // guard let data = try? Data(contentsOf: url)     else { fatalError("could not get data") }
             getFromWeb { [weak self] (result) in
                 guard let self = self else { return }
                 switch result {
                 case .success(let data):
                     
                     do { try data.write(to: self.newFileURL) }
                     catch  {
                         print(error.localizedDescription)
                         fatalError(" could not write the data")
                     }
                     DispatchQueue.main.async {
                         do {
//                             let data = try Data(contentsOf: self.newFileURL)
//                             self.studentPic.image = UIImage(data: data)
                         }
                         catch  {
                             print(error.localizedDescription)
                         }
                         return
                     }
                     
                 case .failure(_):
                     
                     print("error")
                     return
                 }
            }
         }

         do {
             let data = try Data(contentsOf: newFileURL)
             studentPic.image = UIImage(data: data)
         } catch  {
             print(error.localizedDescription)
         }

        
        
        
        let newFileURL = url.appendingPathComponent(url.lastPathComponent)
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }

    
    func getFromWeb(thenWithResultDo completionHandler: @escaping (Result<Data,Error>) -> Void)  {
        
        let session: URLSession = {
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            return session
        }()
        
        let request: URLRequest = {
            /// create request
            var request = URLRequest(url: url)
            /// modify properties
            request.httpMethod = "GET"
            return request
        }()
        
        /* Start a new Task */
        let task = session.dataTask(with: request)  { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            /// check if it was a success
            guard let data = data,(error == nil) else { fatalError("Failed at the task getting the profiles") }
            
            // Success - contine
            let statusCode = (response as! HTTPURLResponse).statusCode
            print("URL Session Task Succeeded: HTTP \(statusCode)")
            completionHandler(Result.success(data))
        }
        task.resume()
        session.finishTasksAndInvalidate()
    }

*/
    
    
    
    func load(url: URL) {
        let newFileURL = url.appendingPathComponent(url.lastPathComponent)
        
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
