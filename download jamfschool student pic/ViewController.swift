//
//  Created by Steven Hertz on 1/4/21.
//
import UIKit

enum URLError: Error {
    case unexpectedError
}

class ViewController: UIViewController {
    
    var studentsOfClass = StudentsOfClass()
    var theClassReturnObject: ClassReturnObject?

    var getAStudentPictute: GetAStudentPictute = GetAStudentPictute()
    //  MARK: -  URL Stuff
    var url: URL = URL(string: "https://manage.zuludesk.com/storage/public/1049131/photos/647bba344396e7c8170902bcf2e15551.jpg")!
    
    //  MARK: -  Stuff for the file manager
    private var fileManger: FileManager = FileManager.default
    lazy var docURL: URL = {
        return  fileManger.urls(for: .documentDirectory, in: .userDomainMask).first!
    }()
    
    //  MARK: -  Outlets
    @IBOutlet weak var studentPic: UIImageView!
    
    @IBOutlet weak var tableView: UITableView!
        
    
    @IBOutlet weak var theStudentPic: UIImageView!
    
    
    
    //  MARK: -
    //  MARK: -  Functions start here
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // self.theStudentPic.image = UIImage(named: "testimage.")

        print("*-* view did load \(HelperStuf.getTimeStamp()) ")

        // self.tableView.dataSource = self
        

        studentsOfClass.getTheClassFromWeb {
            let randomStudent = self.studentsOfClass.theClassReturnObject?.class.students.randomElement()
            guard let photoURL = randomStudent?.photo else   { fatalError("could not unwrap url")}
            self.url = photoURL
            print("this is the updated URL", self.url.path)
            
            let lastPath: String = { self.url.lastPathComponent}()
            let newFileURL: URL = { self.docURL.appendingPathComponent(lastPath) }()
            

            
            self.getAStudentPictute.retreiveDataAsPictureFle(withURL: newFileURL) { data in
                DispatchQueue.main.async {
                   let replaceImage = UIImage(data: data)
                   self.theStudentPic.image = replaceImage
                }
            }
            
        }
        print("stop here")
    }
    
    //  MARK: -  Get Class From Jamf
    
    fileprivate func getTheClassFromWeb() {
        
        let (session, request) = prepareGetClassRequest()
        
        getFromWeb(sessionToUse: session, urlRequstToUse: request)  { [weak self] (result) in
            guard let self = self else { return }
            // get the data from the Result type
            guard let data = try? result.get() else {
                if case Result.failure(let error) = result {
                    print("these was an error with getting on-line pic\(error.localizedDescription)")
                }
                return
            }
            
            // OK we are fine, we got data - so lets write it to a file so we can retieve it
            self.processTheData(with: data)
            return
        }
    }
    
    fileprivate func processTheData(with data: Data) {
        let decoder = JSONDecoder()

        // convert the json to a model
        guard let classReturnObject = try? decoder.decode(ClassReturnObject.self, from: data) else {fatalError("cc")}
        
        theClassReturnObject = classReturnObject
        /*
        let randomStudent = classReturnObject.class.students.randomElement()
        guard let photoURL = randomStudent?.photo else   { fatalError("could not unwrap url")}
        url = photoURL
        print("this is the updated URL", url.path)
        retreiveDataAsPictureFle(withURL: photoURL, theImageView: studentPic) { data in
            print("hello")
        }
 */
        DispatchQueue.main.async {
            print("mock reload date")
//            self.tableView.reloadData()
        }
       

    }
    
    
    func prepareGetClassRequest() -> (URLSession, URLRequest) {
        // get the students record from on-line
        let urlRequest: URLRequest = {
            guard let url = URL(string: "https://api.zuludesk.com/classes/3813b0d4-280f-4a3d-ab55-a8274bc9ead6") else {fatalError("zz")}
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            
            // Headers
            request.addValue("Basic NTM3MjI0NjA6RVBUTlpaVEdYV1U1VEo0Vk5RUDMyWDVZSEpSVjYyMkU=", forHTTPHeaderField: "Authorization")
            request.addValue("3", forHTTPHeaderField: "X-Server-Protocol-Version")
            request.addValue("Hash=f59c9e4a0632aed5aa32c482301cfbc0; hash=78be3e9f9fb5aff8587c93c7a3b3b5f1", forHTTPHeaderField: "Cookie")
            return request
        }()
        let session: URLSession = {
             let sessionConfig = URLSessionConfiguration.default
             let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
             return session
         }()

        return (session, urlRequest)
    }
  

    //  MARK: -  Get the picture file
    
    fileprivate func retreiveDataAsPictureFle(withURL url: URL, completionHandler: @escaping (Data) -> Void) {
        print("*-* retreiveDataAsPictureFle \(HelperStuf.getTimeStamp()) ")

        let lastPath: String = { url.lastPathComponent}()
        let urlForLocalFile: URL = { docURL.appendingPathComponent(lastPath) }()

        if !fileManger.fileExists(atPath: urlForLocalFile.path) {
            
            let (session, request) = prepareStudentPhotoRequest()
            
            //getFromWeb { [weak self] (result) in
            getFromWeb(sessionToUse: session, urlRequstToUse: request)  { [weak self] (result) in
                
                guard let self = self else { return }
                
                // get the data from the Result type
                guard let data = try? result.get() else {
                    if case Result.failure(let error) = result {
                        print("these was an error with getting on-line pic\(error.localizedDescription)")
                    }
                    return
                }
                
                // OK we are fine, we got data - so lets write it to a file so we can retieve it
                let image = UIImage(data: data)
                let targetSize = CGSize(width: 120, height: 90)

                let scaledImage = image!.scalePreservingAspectRatio(
                    targetSize: targetSize
                )
                let scaledData = scaledImage.jpegData(compressionQuality: 0.70)
                
                do { try scaledData?.write(to: urlForLocalFile) }
                catch  {
                    print(error.localizedDescription)
                    fatalError(" could not write the data")
                }
                self.readPicFileFromLocalDataFile(withURL: urlForLocalFile, completionHandler: completionHandler)
                return
            }
        }
        print("*-*  readPicFileFromLocalDataFileandDisplay \(HelperStuf.getTimeStamp()) ")
        readPicFileFromLocalDataFile(withURL: urlForLocalFile, completionHandler: completionHandler)
    }
    
    
    fileprivate func readPicFileFromLocalDataFile(withURL urlForLocalFile: URL,  completionHandler: @escaping (Data) -> Void) {
         DispatchQueue.main.async {
             do {
                 let data = try Data(contentsOf: urlForLocalFile)
                completionHandler(data)

             } catch  {
                 print(error.localizedDescription)
             }
         }
     }
  
    func prepareStudentPhotoRequest() -> (URLSession, URLRequest) {
        let session: URLSession = {
            let sessionConfig = URLSessionConfiguration.default
            let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
            return session
        }()
        
        let request: URLRequest = {
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            return request
        }()
        
        return (session, request)
    }

    
    //  MARK: -  Get  data from web
    func getFromWeb(sessionToUse session: URLSession, urlRequstToUse request: URLRequest,  thenWithResultDo completionHandler: @escaping (Result<Data,Error>) -> Void)  {
        
        //  FIXME: Handle the different type of possible error codes that can occur
        
        /* Start a new Task */
        let task = session.dataTask(with: request)  { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            
            let statusCode = (response as! HTTPURLResponse).statusCode
            print("URL Session Task Succeeded: HTTP \(statusCode)")

            let result = Result<Data,Error> {
                if let error = error {
                    throw error
                } else if let data = data {
                    return data
                } else {
                    throw URLError.unexpectedError
                }
            }
            completionHandler(result)
            
        }
        task.resume()
        session.finishTasksAndInvalidate()
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(" in will display ")
      
        
        guard let theClassReturnObject = studentsOfClass.theClassReturnObject else { return  }
        
        print(" in will display ")

        let photoURL = theClassReturnObject.class.students[indexPath.row].photo
       
        url = photoURL
        let lastPath: String = { url.lastPathComponent}()
        let newFileURL: URL = { docURL.appendingPathComponent(lastPath) }()

        
        print(newFileURL.path)
        retreiveDataAsPictureFle(withURL: newFileURL) { data in
            
            guard let studentIndex = self.studentsOfClass.theClassReturnObject?.class.students.firstIndex(where: { (student) -> Bool in
                student.photo == photoURL
            }) else { fatalError("oops")}

            let studentIndexPath = IndexPath(item: studentIndex, section: 0)
            guard let cell = tableView.cellForRow(at: studentIndexPath) else { fatalError("no index path")}
            cell.imageView?.image = UIImage(data: data)
            cell.textLabel?.text = self.studentsOfClass.theClassReturnObject?.class.students[indexPath.row].lastName
        }

    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let theClassReturnObject = studentsOfClass.theClassReturnObject else { return 0 }
        return (theClassReturnObject.class.students.count)

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for: indexPath)
        /*
//        cell.textLabel?.text = "hello"
        guard let theClassReturnObject = theClassReturnObject else { return cell }
        cell.textLabel?.text = theClassReturnObject.class.students[indexPath.row].lastName
  //      cell.imageView?.image = UIImage(imageLiteralResourceName: "testImage")
  //      retreiveDataAsPictureFle(withURL: newFileURL, theImageView: cell.imageView!)
        
        let photoURL = theClassReturnObject.class.students[indexPath.row].photo
       
        url = photoURL
        let lastPath: String = { url.lastPathComponent}()
        let newFileURL: URL = { docURL.appendingPathComponent(lastPath) }()

        print(newFileURL.path)
        retreiveDataAsPictureFle(withURL: newFileURL, theImageView: cell.imageView!) { data in
            cell.imageView?.image = UIImage(data: data)
        }
*/
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("row selected")
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 180.0
//    }
    
}

//extension BookDataSource: UITableViewDataSource {
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return book?.text.count ?? 0
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: LineCell.reuseIdentifier, for: indexPath)
//        configure(cell: cell, indexPath: indexPath)
//        return cell
//    }
//
//    private func configure(cell: UITableViewCell, indexPath: IndexPath) {
//        if let cell = cell as? LineCell {
//            cell.lineLabel.text = NumberFormatter.localizedString(from:
//                NSNumber(value: indexPath.row + 1), number: .decimal)
//            cell.textView.text = book?.text[indexPath.row]
//        }
//    }
//}
